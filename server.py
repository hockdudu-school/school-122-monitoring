#!/usr/bin/python3

import asyncio
import json
import socket
import sys
import time
import urllib.error
import urllib.request

from asyncio.base_events import Server
from asyncio.exceptions import IncompleteReadError
from asyncio.streams import StreamReader, StreamWriter
from configparser import ConfigParser
from enum import Enum
from threading import Event, Thread


####################
# Server Internals #
####################


SOCK_REQUEST_STATUS = b'status'
SOCK_REQUEST_LOG = b'log'
SOCK_REQUEST_ERROR_LOG = b'error_log'

last_status = {}
log = []
error_log = []

stop_event: Event
server: Server


class ServerStatus(Enum):
    OK = 'ok'
    DOWN = 'down'
    TIMEOUT = 'timeout'


def add_to_list_with_limit(data, list: list, limit: int):
    list.append(data)
    if len(list) > limit:
        list.pop(0)


def save_status(server_name: str, status: ServerStatus):
    last_status[server_name] = status
    data = {
        'server': server_name,
        'status': status,
        'time': int(time.time())
    }

    add_to_list_with_limit(data, log, config['log_max_size'])

    if status != ServerStatus.OK:
        add_to_list_with_limit(data, error_log, config['error_log_max_size'])


class BGColor(Enum):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def printc(text: str, *colors: BGColor, end: str = '\n'):
    print(f'{"".join(map(lambda color: color.value, colors))}{text}{BGColor.ENDC.value}', end=end)


def get_config_dict():
    config = ConfigParser(default_section='common')
    config.read('./config.ini')

    server_list_file = config.get('common', 'server_list')

    with open(server_list_file) as file:
        content = file.readlines()
    content = [line.strip() for line in content]

    return {
        'servers': content,
        'host': config.get('server', 'host'),
        'port': config.get('server', 'port'),
        'interval': config.getint('common', 'interval'),
        'timeout': config.getint('common', 'timeout'),
        'log_max_size': config.getint('common', 'log_max_size'),
        'error_log_max_size': config.getint('common', 'error_log_max_size'),
    }


def check_websites():
    printc(f'Checking servers... Time: {time.ctime()}', BGColor.HEADER, BGColor.BOLD, BGColor.UNDERLINE)
    for server in config['servers']:
        if stop_event.is_set():
            return

        print(f'Checking {server}: ', end='')
        try:
            req = urllib.request.Request(server, headers={'User-Agent': 'Mozilla/5.0'})
            with urllib.request.urlopen(req, timeout=config['timeout']):
                printc('OK', BGColor.BOLD, BGColor.OKGREEN)
                save_status(server, ServerStatus.OK)

        except urllib.error.HTTPError as e:
            printc(f'HTTP Error, code {e.code}', BGColor.BOLD, BGColor.FAIL)
            save_status(server, ServerStatus.DOWN)

        except socket.timeout:
            printc('Timeout', BGColor.BOLD, BGColor.FAIL)
            save_status(server, ServerStatus.TIMEOUT)

        except urllib.error.URLError as e:
            printc(f'URL error: {e}', BGColor.BOLD, BGColor.FAIL)
            save_status(server, ServerStatus.DOWN)

        except ValueError as e:
            printc(f'Value Error: {e}', BGColor.BOLD, BGColor.FAIL)
            save_status(server, ServerStatus.DOWN)

    print()


def scheduler_run():
    while not stop_event.is_set():
        start = time.time()
        check_websites()
        diff = time.time() - start

        next_execution_time = time.time() + config['interval'] - diff

        while time.time() < next_execution_time and not stop_event.is_set():
            time.sleep(.1)


####################
# Client functions #
####################


async def client_connected(reader: StreamReader, writer: StreamWriter):
    try:
        data = await reader.readuntil(b'\0')
        data = data[:-1]

        result = ''

        if data == SOCK_REQUEST_STATUS:
            result = client_sock_request_status()

        if data == SOCK_REQUEST_LOG:
            result = client_sock_request_log()

        if data == SOCK_REQUEST_ERROR_LOG:
            result = client_sock_request_error_log()

        writer.write(result.encode('utf-8') + b'\0')

        await writer.drain()

    except IncompleteReadError:
        pass


def client_sock_request_status():
    status_dict = {}

    for key in last_status:
        status_dict[key] = last_status[key].value

    return json.dumps(status_dict)


def client_sock_request_log():
    log_list = []
    for log_item in log:
        log_list.append(convert_log_item_to_dict(log_item))

    return json.dumps(log_list)


def client_sock_request_error_log():
    log_list = []
    for log_item in error_log:
        log_list.append(convert_log_item_to_dict(log_item))

    return json.dumps(log_list)


def convert_log_item_to_dict(log_item: dict):
    return {
        'server': log_item['server'],
        'status': log_item['status'].value,
        'time': log_item['time']
    }

#############
# Main loop #
#############


config = get_config_dict()


async def main():
    global stop_event
    global server

    print(f'🚀 Starting server, listening on {config["host"]}:{config["port"]}\n')
    server = await asyncio.start_server(client_connected, config['host'], config['port'])

    stop_event = Event()

    scheduler_thread = Thread(target=scheduler_run)
    scheduler_thread.start()

    await server.wait_closed()


try:
    asyncio.run(main())
except:
    try:
        stop_event.set()
    except NameError:
        pass

    try:
        server.close()
    except NameError:
        pass

    sys.exit(1)
