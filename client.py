#!/usr/bin/python3

import asyncio
import json
import time

from argparse import ArgumentParser

SOCK_REQUEST_STATUS = b'status'
SOCK_REQUEST_LOG = b'log'
SOCK_REQUEST_ERROR_LOG = b'error_log'

parser = ArgumentParser()
parser.add_argument('command', help='The command to execute', choices=['status', 'log', 'error_log'])
parser.add_argument('-s', '--server', dest='server', help='The server to connect to', default='localhost')
parser.add_argument('-p', '--port', dest='port', help='The port to connect to', default='3256')
parser.add_argument('-f', '--format', dest='format', help='The format to output', choices=['json', 'text'],
                    default='text')
args = parser.parse_args()


############
# Commands #
############

class CommandStatus(object):
    @staticmethod
    def type():
        return SOCK_REQUEST_STATUS

    @staticmethod
    def format(input: str):
        if args.format == 'json':
            return input

        dict = json.loads(input)
        if args.format == 'text':
            out = []
            for key in dict:
                out.append(f'{key}: {dict[key]}')

            return '\n'.join(out)

        raise ValueError(f'Unknown format type "{args.format}"')


class CommandLog(object):
    @staticmethod
    def type():
        return SOCK_REQUEST_LOG

    @staticmethod
    def format(input: str):
        if args.format == 'json':
            return input

        list = json.loads(input)
        if args.format == 'text':
            out = []
            for dict in list:
                out.append(f'{time.ctime(int(dict["time"]))}, {dict["server"]}: {dict["status"]}')

            return '\n'.join(out)

        raise ValueError(f'Unknown format type "{args.format}"')


class CommandErrorLog(CommandLog):
    @staticmethod
    def type():
        return SOCK_REQUEST_ERROR_LOG


########
# Main #
########

def get_commands():
    if args.command == 'status':
        return CommandStatus()
    if args.command == 'log':
        return CommandLog()
    if args.command == 'error_log':
        return CommandErrorLog()

    raise ValueError(f'Unknown command "{args.command}"')


async def main():
    command = get_commands()
    reader, writer = await asyncio.open_connection(args.server, args.port)

    writer.write(command.type() + b'\0')
    await writer.drain()

    data = await reader.readuntil(b'\0')
    data = data[:-1]

    print(command.format(data.decode('utf-8')))


asyncio.run(main())
